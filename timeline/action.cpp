#include "action.h"
#include "track.h"

#include <QDebug>

const int int_max = std::numeric_limits<int>::max();


Action::Action()
    : Action( 0, 0 )
{
}


Action::Action( int beg, int duration )
    : QObject(nullptr), _beg(beg), _end(beg + duration), _min_duration(1000), _max_duration(std::numeric_limits<int>::max() ),
        _duration_modify(true), _position_modify(true), _select(false), _track(nullptr)
{
}

int Action::maxBegTime() const
{
    return endTime() - minDuration();
}

int Action::minBegTime() const
{
    int index = -1, min_time = 0;
    if( _track == nullptr || ( index = _track->findAction( this ) ) <= 0 ) {
        min_time = endTime() - maxDuration() < 0 ? 0 : endTime() - maxDuration();
    }
    else
    {
        for( int i = index - 1; i >= 0; --i )
        {
            int duration = _track->action( i )->duration();
            min_time += duration;
        }
    }
    return min_time;
}

void Action::setBegTime(int beg)
{
    if( !_position_modify )
        return;

    int min = minBegTime(), max = maxBegTime();
    if     ( beg >= max )
        beg = max;
    else if( beg <= min )
        beg = min;

    int first_index = 0, last_index = 0, pos = beg, current_duration = _end - beg;
    if( _track != nullptr && intersectActions( pos, current_duration, first_index, last_index) >= 0 )
    {
        int total_length = 0, free_space = 0;

        bool prev = true, stop = false;
        for( int i = last_index; i >= 0 && prev && ! stop; --i )
        {
            free_space = 0;

            Action* item = _track->action( i );
            if( i != 0 )
            {
                Action* prev_item = _track->action( i - 1 );
                free_space = item->begTime() - prev_item->endTime();
            }
            else
            {
                free_space = item->begTime();
                if( free_space <= 0 )
                    stop = true;
            }

            prev = free_space < ( _track->action(last_index)->endTime() - beg );

            int duration = item->duration();
            item->_beg = beg - duration - total_length;
            item->_end = item->_beg + duration;

            total_length += duration;
        }
    }
    this->_beg = beg;

    emit resized();
}

int Action::minEndTime() const
{
    return begTime() + minDuration();
}

int Action::maxEndTime() const
{
    int index = -1, max_time = int_max;
    if( _track == nullptr || ( index = _track->findAction( this ) ) < 0 || index == _track->actionsNumber() - 1 ) {
        max_time = maxDuration() < int_max - begTime() ? begTime() + maxDuration() : int_max;
    }
    else
    {
        for( int i = index + 1; i < _track->actionsNumber(); ++i )
        {
            int duration = _track->action( i )->duration();
            max_time -= duration;
        }
    }
    return max_time;
}

void Action::setEndTime(int end)
{
    if( !_duration_modify )
        return;

    int min = minEndTime(), max = maxEndTime();
    if      ( end >= max )
        end = max;
    else if ( end <= min )
        end = min;

    int first_index = 0, last_index = 0, pos = _beg, current_duration = end - _beg;
    if( _track != nullptr && intersectActions( pos, current_duration , first_index, last_index ) >= 0 )
    {
        int total_length = 0, free_space = 0;

        bool next = true;
        for( int i = first_index; i < _track->actionsNumber() && next; ++i )
        {
            free_space = 0;

            Action* item = _track->action( i );
            if( i != _track->actionsNumber() - 1 )
            {
                Action* next_item = _track->action( i + 1 );
                free_space = next_item->begTime() - item->endTime();
            }
            else
                free_space = int_max - item->endTime();

            next = free_space < ( end - _track->action(first_index)->begTime() );
            qDebug() << next;

            int duration = item->duration();
            item->_beg = end + total_length;
            item->_end = item->_beg + duration;

            total_length += duration;
        }
    }
    this->_end = end;

    emit resized();
}

void Action::setDurationModify(bool modify)
{
    this->_duration_modify = modify;
}

void Action::setPositionModify(bool modify)
{
    this->_position_modify = modify;
}

void Action::setMinDuration(int duration)
{
    if( duration > min_duration && duration < _max_duration )
        this->_min_duration = duration;
    else if( duration <= min_duration )
        this->_min_duration = min_duration;
    else
        this->_min_duration = _max_duration;
}

void Action::setMaxDuration(int duration)
{
    if( duration > _min_duration && duration < std::numeric_limits<int>::max() )
        this->_max_duration = duration;
    else if( duration <= _min_duration )
        this->_max_duration = _min_duration;
    else
        this->_max_duration = std::numeric_limits<int>::max();
}

void Action::move( int pos )
{
    if( ! _position_modify )
        return;

    if( pos > int_max - duration() )
        pos = int_max - duration();
    else if ( pos < 0 )
        pos = 0;

    int new_pos = 0, first_index = 0, last_index = 0, current_duration = duration();
    if( _track != nullptr && intersectActions( pos, current_duration, first_index, last_index ) >= 0 )
    {
        int jump = 0;

        int curr_index = _track->findAction( this );
        if( pos < _beg )
        {
            int first_moved_action;

            Action* first_action = _track->action( first_index );
            if( pos > first_action->begTime() )
            {
                new_pos = first_action->endTime();
                first_moved_action = first_index + 1;
            }
            else
            {
                new_pos = pos;
                first_moved_action = first_index;
            }
            for( int i = first_moved_action; i <= last_index; ++i )
            {
                Action* act = _track->action( i );
                if( act == this )
                    continue;

                int duration = act->duration();
                act->_beg = act->_beg + current_duration;
                act->_end = act->_beg + duration;

                ++jump;
            }

            if( jump > 0 )
                jumped( curr_index, curr_index - jump );
        }
        else
        {
            int last_moved_action;

            Action* last_action = _track->action( last_index );
            if( pos + current_duration < last_action->endTime() )
            {
                new_pos = last_action->begTime() - current_duration;
                last_moved_action = last_index - 1;
            }
            else
            {
                new_pos = pos;
                last_moved_action = last_index;
            }
            for( int i = last_moved_action; i >= first_index; --i )
            {
                Action* act = _track->action( i );
                if( act == this )
                    continue;

                int duration = act->duration();
                act->_beg = new_pos - duration;
                act->_end = act->_beg + duration;

                ++jump;
            }
            if( jump > 0 )
                jumped( curr_index, curr_index + jump );
        }
    }
    else
        new_pos = pos;

    _beg = new_pos;
    _end = new_pos + current_duration;

    emit moved();
}

void Action::setSelect( bool select )
{
    _select = select;
    emit selected();
}

int Action::intersectActions( int pos, int duration, int& first_index, int& last_index ) const
{
    int result = -1;

    first_index = last_index = -1;
    for( int i = 0; i < _track->actionsNumber(); i++ )
    {
        Action* item = _track->action(i);
        if( item == this )
            continue;

        if( pos < item->endTime() && pos + duration > item->begTime() )
        {
            if( first_index < 0 )
            {
                first_index = i;
                result = 0;
            }
            else
                last_index = i;

            ++result;
        }
    }
    if( last_index < 0 )
        last_index = first_index;

    return result;
}

bool Action::isCross(Action *action) const
{
    return begTime() < action->endTime() && endTime() > action->begTime();
}

void Action::setTrack( Track* track )
{
    if( track == nullptr)
        return;

    this->setParent( track );
    this->_track = track;

    if( _beg != 0 || _end != 0 )
    {
        int dur = duration();

        setBegTime( _beg );
        setEndTime( _beg + dur );
    }
}
