#ifndef ACTION_H
#define ACTION_H

#include <QObject>

const int min_duration = 1000;

class Track;

class Action: public QObject
{
    Q_OBJECT

    friend class Track;
public:

    Action();
    Action( int beg, int duration );

    void setBegTime( int beg );
    int begTime() const { return _beg; }
    void setEndTime( int end );
    int endTime() const { return _end; }
    void move( int pos );

    void setSelect( bool select );
    bool select() const { return _select; }

    void setDurationModify( bool modify );
    bool durationModify() const { return _duration_modify; }
    void setPositionModify( bool modify );
    bool positionModify() const { return _position_modify; }
    int duration() const { return _end - _beg; }

    void setMinDuration( int duration );
    int minDuration() const { return _min_duration; }
    void setMaxDuration( int duration );
    int maxDuration() const { return _max_duration; }
    Track* track() { return _track; }
    bool isCross( Action* action ) const;

signals:
    void resized();
    void moved();
    void selected();
    void jumped( int pos, int new_pos );

private:
    int intersectActions( int pos, int duration, int& first_index, int& last_index ) const;
    int maxBegTime() const;
    int minBegTime() const;
    int maxEndTime() const;
    int minEndTime() const;
    void setTrack( Track* track );

    int _beg, _end, _min_duration, _max_duration;
    bool _duration_modify, _position_modify, _select;
    Track* _track;
};

#endif // ACTION_H
