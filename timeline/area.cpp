#include "area.h"
#include "sizes.h"
#include "track.h"
#include "action.h"
#include "rule.h"
#include "timeline.h"
#include <QtCore/qmath.h>
#include <QWidget>

Area::Area(QObject *parent) : QObject(parent), _shift(0), _current_time(0), _x(0), _y(0), _width(0), _height(0)
{
}

void Area::paint(QPainter& painter)
{
    this->paintBackground( painter );
    this->paintTools( painter );
    this->paintLines( painter );
    this->paintActions( painter );
    if( _tracks.size() > 0 ) {
        this->paintSlider( painter );
    }
}

void Area::paintTools( QPainter& painter )
{
    painter.setClipRect( QRectF( parentX(1), parentY(1), tool_width, _height - 1 ) );
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];

        QFontMetrics fm( parent()->font() );
        QSize text_size = fm.size(Qt::TextSingleLine, track->name() );

        if( ! track->select() )
            painter.setBrush( QBrush( QColor( 240, 240, 240 ) ) );
        else
            painter.setBrush( QBrush( QColor( 200, 200, 200 ) ) );

        painter.drawRect( QRectF( parentX(0), numToY( track_number ), tool_width, tool_height ) );
        painter.drawText( QPointF( parentX(0 + tool_width / 2 - text_size.width() / 2 ), numToY( track_number ) + tool_height / 2 + text_size.height() / 2 ), track->name() );

    }
    painter.setClipRect( QRectF(), Qt::ClipOperation::NoClip );
}

void Area::paintLines( QPainter& painter )
{
    painter.setClipRect( QRectF( parentX(tool_width + 1), parentY(1), _width - tool_width - 1, _height - 1 ) );
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        painter.setBrush( QBrush( QColor( 240, 240, 240 ) ) );
        if( _rule->msecToX( _rule->duration() ) + _rule->lastShift() > parentX( _width ) )
            painter.drawRect( QRectF( parentX(tool_width), numToY( track_number ), _width - tool_width, tool_height ) );
        else
            painter.drawRect( QRectF( parentX(tool_width), numToY( track_number ), thisX( _rule->msecToX( _rule->duration() ) ) + _rule->lastShift() - tool_width, tool_height ) );
    }
    painter.setClipRect( QRectF(), Qt::ClipOperation::NoClip );
}

void Area::paintActions( QPainter& painter )
{
    painter.setClipRect( QRectF( parentX(tool_width + 1), parentY(1), _width - tool_width - 1, _height - 1 ) );
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];

        for( int action_number = 0; action_number < track->actionsNumber(); action_number++ )
        {
            Action* action = track->action(action_number);

            if( ! action->select() )
                painter.setBrush( QBrush( QColor( 166, 166, 166 ) ) );
            else
                painter.setBrush( QBrush( QColor( 100, 100, 100 ) ) );

            if( parentX(tool_width) < _rule->msecToX( action->endTime() ) && _rule->msecToX( action->begTime() ) < parentX(_width) ) {
                painter.drawRect( QRectF( _rule->msecToX( action->begTime() ), numToY( track_number ), _rule->msecToX( action->endTime() ) - _rule->msecToX( action->begTime() ), tool_height ) );
            }
        }
    }
    painter.setClipRect( QRectF(), Qt::ClipOperation::NoClip );
}

void Area::paintSlider( QPainter& painter )
{
    double pos = _rule->msecToX( _current_time );
    painter.setClipRect( QRectF( parentX(tool_width + 1), parentY(-5), _width - tool_width, _height - 5 ) );

    if( pos > parentX( tool_width - 5 )  && pos < parentX( _width + 5 ) )
    {
        double height = this->_height < _tracks.size() * tool_height ? this->_height : _tracks.size() * tool_height;

        QPolygonF top_polygon, bottom_polygon;
        top_polygon << QPointF( pos - 5, parentY(-5) ) << QPointF( pos, parentY(0) ) << QPointF( pos + 5, parentY(-5) );
        bottom_polygon << QPointF( pos - 5,  parentY(height + 5) ) << QPointF( pos + 5, parentY(height + 5) ) << QPointF( pos, parentY(height) );

        painter.setBrush( Qt::black );

        painter.drawPolygon( top_polygon );
        painter.drawLine( QPointF( pos, parentY(0)), QPointF( pos, parentY( height ) ) );
        painter.drawPolygon( bottom_polygon );
    }
    painter.setClipRect( QRectF(), Qt::ClipOperation::NoClip );
}

void Area::paintBackground(QPainter &painter)
{
    painter.setClipRect(QRectF(parentX(0), parentY(0), _width + 1, _height + 1));

    painter.setPen(Qt::NoPen);
    int num = qFloor(_height) / tool_height + 1 ;

    int start_val = _shift / tool_height;
    for( int i = start_val; i <= start_val + num; i++ )
    {
        QColor color;
        if( i % 2 != 0 )
            color.setRgb(160,160,160);
        else
            color.setRgb(180,180,180);

        painter.setBrush(QBrush(color, Qt::SolidPattern));
        painter.drawRect(QRectF( parentX(0), parentY( (i - start_val) * tool_height - _shift % tool_height ), tool_width, tool_height));

        if( i % 2 != 0 )
            color.setRgb(200,200,200);
        else
            color.setRgb(220,220,220);

        painter.setBrush(QBrush(color, Qt::SolidPattern));
        painter.drawRect(QRectF(parentX(tool_width), parentY((i - start_val) * tool_height - _shift % tool_height), _width - tool_width, tool_height));

    }
    painter.setBrush(Qt::NoBrush);
    painter.setPen(Qt::SolidLine);
    painter.drawRect(QRectF(parentX(0), parentY(0), _width, _height));

    painter.setClipRect(QRectF(), Qt::ClipOperation::NoClip);
}


void Area::setShift(int shift)
{
    if( shift > 0 && shift <= maxShift() )
        _shift = shift;
    else if( shift > maxShift() )
        _shift = maxShift();
    else
        _shift = 0;

    this->doPaint();
}

int Area::maxShift() const
{
    int value = _tracks.size() * tool_height - qFloor(_height);
    if( value > 0 )
        return value;
    else
        return 0;
}

int Area::tracksNumber() const
{
    return _tracks.size();
}

void Area::setRule( Rule* rule )
{
    this->_rule = rule;
}

int Area::addTrack( Track* track )
{
    track->setParent( this );
    _tracks.push_back( track );
    connect( track, SIGNAL( changed() ), this, SLOT( trackChanged() ) );

    _rule->setDuration( this->maxDuration() );
    return _tracks.size() - 1;
}

void Area::insertTrack( int position, Track* track )
{
    track->setParent( this );
    _tracks.insert( position, track );
    connect( track, SIGNAL( changed() ), this, SLOT( trackChanged() ) );

    _rule->setDuration( this->maxDuration() );
}

Track* Area::track( int num ) const
{
    return _tracks[ num ];
}

int Area::findTrack( Track* track )
{
    QList<Track*>::const_iterator it = std::find( _tracks.begin(), _tracks.end(), track );
    if(it != _tracks.end() )
        return it - _tracks.begin();
    else
        return -1;
}

void Area::removeTrack( Track* track )
{
    int index = findTrack( track );
    if( index != -1 )
    {
        track->setParent( nullptr );
        _tracks.removeOne( track );
        disconnect( track, SIGNAL( changed() ), this, SLOT( trackChanged() ) );
        // delete track;
    }
}

void Area::setCurrentTime( int time )
{
    if( time >= 0 && time <= _rule->duration() )
        _current_time = time;
    else if( time < 0 )
        _current_time = 0;
    else
        _current_time = _rule->duration();

    this->doPaint();
}

int Area::findAction(Action* action) const
{
    int count = 0;
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];
        int result = track->findAction( action );
        if( result >= 0)
            return count + result;

        count += track->actionsNumber();
    }
    return -1;
}

Action* Area::action( int num ) const
{
    int count = 0;
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];
        for( int action_number = 0; action_number < track->actionsNumber(); action_number++ )
        {
            Action* action = track->action( action_number );
            if( count == num )
                return action;

            count++;
        }
    }
    return nullptr;
}

int Area::actionsNumber() const
{
    int sum = 0;
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];
        sum += track->actionsNumber();
    }
    return sum;
}

double Area::numToY( int num ) const
{
    return parentY( 0 - _shift + tool_height * num );
}


QList<QRectF> Area::actionsRect() const
{
    QList<QRectF> rects;

    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];
        for( int action_number = 0; action_number < track->actionsNumber(); action_number++ )
        {
            Action* action = track->action( action_number );

            double beg_x = _rule->msecToX( action->begTime() );
            double end_x = _rule->msecToX( action->endTime() );
            double y = numToY( track_number );

            QRectF rect( beg_x, y, end_x - beg_x, tool_height );
            rects.push_back( rect );
        }
    }
    return rects;
}

QList<QRectF> Area::toolsRect() const
{
    QList<QRectF> rects;

    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        QRectF rect( parentX(0), numToY( track_number), tool_width, tool_height );
        rects.push_back( rect );
    }
    return rects;
}

QList<QRectF> Area::linesRect() const
{
    QList<QRectF> rects;

    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        QRectF rect( parentX( tool_width ), numToY( track_number ), _width - tool_width, tool_height );
        rects.push_back( rect );
    }
    return rects;
}

void Area::trackChanged()
{
    int max_duration = this->maxDuration();

    _rule->setDuration( max_duration );
    if( _current_time > max_duration )
        _current_time = max_duration;

    this->doPaint();
    emit changed();
}

void Area::setGeometry(int x, int y, int w, int h)
{
    this->doResize( w, h, false );
    this->doMove( x, y, false );

    this->doPaint();
}

void Area::resize( int w, int h )
{
    this->doResize( w, h, true );
}

void Area::move( int x, int y )
{
    this->doMove( x, y, true );
}

QWidget* Area::parent() const
{
    return qobject_cast<QWidget*>( QObject::parent() );
}

double Area::parentX( double x ) const
{
    return this->_x + x;
}

double Area::parentY( double y ) const
{
    return this->_y + y;
}

double Area::thisX( double x ) const
{
    return x - this->_x;
}

double Area::thisY( double y ) const
{
    return y - this->_y;
}

void Area::doResize(int w, int h, bool repaint)
{
    this->_width = w;
    this->_height = h;
    if( _shift > maxShift() ) {
        _shift = maxShift();
    }
    if( repaint ) {
        this->doPaint();
    }
}

void Area::doMove(int x, int y, bool repaint)
{
    this->_x = x;
    this->_y = y;
    if( repaint ) {
        this->doPaint();
    }
}

void Area::doPaint()
{
    parent()->repaint();
}

int Area::maxDuration() const
{
    int max_duration = 0;
    for( int track_number = 0; track_number < _tracks.size(); track_number++ )
    {
        Track* track = _tracks[ track_number ];
        for( int action_number = 0; action_number < track->actionsNumber(); action_number++ )
        {
            Action* action = track->action( action_number );
            if( action->endTime() > max_duration )
                max_duration = action->endTime();
        }
    }
    return max_duration;
}
