#ifndef AREA_H
#define AREA_H

#include <QObject>
#include <QPainter>

class Track;
class Rule;
class Action;
class Slider;


class Area : public QObject
{
    Q_OBJECT
public:
    explicit Area(QObject *parent = nullptr);
    void setGeometry( int x, int y, int w, int h);
    void resize( int w, int h );
    double height() { return _height; }
    double width() { return _width; }
    double x() { return _x; }
    double y() { return _y; }
    void move( int x, int y );
    void paint( QPainter& painter );

    void setShift(int shift);
    int maxShift() const;
    int shift() { return _shift; }
    void setRule( Rule* rule );
    Rule* rule() const { return _rule; }

    int addTrack( Track* track );
    void insertTrack( int position, Track* track );
    Track* track( int num ) const;
    int findTrack( Track* track );
    void removeTrack( Track* track );
    int tracksNumber() const;

    int findAction( Action* action ) const;
    Action* action( int num ) const;
    int actionsNumber() const;

    void setCurrentTime( int time );
    int currentTime() const { return _current_time; }
    double numToY( int num ) const;

    QList<QRectF> actionsRect() const;
    QList<QRectF> toolsRect() const;
    QList<QRectF> linesRect() const;

public slots:
    void trackChanged();

signals:
    void changed();

private:
    QWidget* parent() const;
    double parentX( double x ) const;
    double parentY( double y ) const;
    double thisX( double x ) const;
    double thisY( double y ) const;
    void doResize( int w, int h, bool repaint );
    void doMove( int x, int y, bool repaint );
    void doPaint();
    int maxDuration() const;

    void paintTools( QPainter& painter );
    void paintLines( QPainter& painter );
    void paintActions( QPainter& painter );
    void paintSlider( QPainter& painter );
    void paintBackground(QPainter& painter);


    int _shift, _current_time;
    double _x, _y, _width, _height;
    QList<Track*> _tracks;
    Rule* _rule;

};

#endif // AREA_H
