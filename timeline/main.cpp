#include "editing_bench/qediting_bench.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QEditingBench editing_bench;
    editing_bench.show();

    return a.exec();
}
