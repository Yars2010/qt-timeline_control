#include "rule.h"
#include "sizes.h"
#include "timeline.h"
#include <QtCore/qmath.h>
#include <QString>
#include <QFontMetrics>
#include <QWidget>
#include <QDebug>


static const int sec_size = 10;
static const int min_size = 4;


Rule::Rule(QObject *parent) : QObject(parent), _duration(0), _position(0), _zoom(100), _zero_shift(20), _last_shift(20), _x(0), _y(0),  _width(0), _height(0)
{
    section_value << 1 << 5 << 10 << 20 << 30 << 60 << 300 << 600;
}

void Rule::paint( QPainter& painter )
{
    QSize caption_size = this->captionSize();
    int stroke_value = this->strokeValue();
    int caption_multipler = this->captionMultipler();

    painter.setBrush( Qt::NoBrush );
    painter.drawRect( QRectF( parentX(0.0), parentY(0.0), _width, _height ) );
    painter.setClipRect( QRectF(parentX(0.0), parentY(0.0), _width, _height) );

    int start = _position / 1000 - qFloor( static_cast<double>(_zero_shift) / secDistance() );
    if( start < 0 ) start = 0;

    int end = start + visibleDuration() / 1000 + qFloor( static_cast<double>(_last_shift) / secDistance() );
    if( end > _duration / 1000 ) end = _duration / 1000;

    for( int i = start; i <= end; i++  )
    {
        if( i % caption_multipler == 0 || i % stroke_value == 0 )
        {
            double x = msecToX( i * 1000 );

            if( i % caption_multipler == 0 )
            {
                int min = i / 60;
                int sec = i % 60;
                QString min_str = min < 10 ? QString("0%1").arg(min) : QString::number(min);
                QString sec_str = sec < 10 ? QString("0%1").arg(sec) : QString::number(sec);

                painter.drawText( QPointF( x - caption_size.width() / 2, parentY( _height / 2 ) ), QString("%1:%2").arg( min_str, sec_str ) );
                painter.drawLine( QLineF(x, parentY(_height / 2), x, parentY(_height) ) );
            }
            else if( i != _duration / 1000 )
                painter.drawLine( QLineF(x, parentY(_height - _height / 6), x, parentY(_height)) );
            else
                painter.drawLine( QLineF(x, parentY(_height / 2), x, parentY(_height) ) );
        }
    }
    painter.setClipRect( QRectF(), Qt::ClipOperation::NoClip);
}

void Rule::setDuration( int duration )
{
    this->_duration = duration;

    if( this->_position > maxPosition() )
        this->_position = maxPosition();

    this->doPaint();
}

void Rule::setPosition( int position )
{
    if( position > 0 && position <= maxPosition() )
        this->_position = position;
    else if( position > maxPosition() )
        this->_position = maxPosition();
    else
        this->_position = 0;

    this->doPaint();
}

void Rule::setZoom( int zoom )
{
    if( zoom >= 10 && zoom <= 400 ) {
        this->_zoom = zoom;
    }
    else if( zoom < 10 ) {
        this->_zoom = 10;
    }
    else if( zoom > 400 ) {
        this->_zoom = 400;
    }

    if( this->_position > maxPosition() ){
        this->_position = maxPosition();
    }

    this->doPaint();
}

void Rule::setGeometry( int x, int y, int w, int h )
{
    this->doMove( x, y, false );
    this->doResize( w, h, false );

    this->doPaint();
}

void Rule::resize( int w, int h )
{
    this->doResize( w, h, true );
}

void Rule::move( int x, int y )
{
    this->doMove( x, y, true );
}

int Rule::maxPosition() const
{
    int value = qCeil( (_width - _zero_shift - _last_shift ) / strokeDistance() * strokeValue() * 1000);
    if( value < _duration )
        return _duration - value;
    else
        return 0;
}

double Rule::msecToX( int msec ) const
{
    double distance = secDistance();
    return parentX( distance * ( msec - _position ) / 1000.0 + _zero_shift );
}

int Rule::xToMsec( double x ) const
{
    double distance = secDistance();
    return qFloor( ( thisX(x) - _zero_shift ) / distance * 1000.0 + _position );
}

int Rule::visibleDuration() const
{
    double distance = secDistance();
    int result = qFloor( _width / distance * 1000);
    if( result > _duration )
        return _duration;
    else
        return result;
}

void Rule::doResize( int w, int h, bool repaint )
{
    this->_width = w;
    this->_height = h;

    if( this->_position > maxPosition() ){
        this->_position = maxPosition();
    }

    if( repaint ) {
        this->doPaint();
    }
}

void Rule::doMove( int x, int y, bool repaint )
{
    this->_x = x;
    this->_y = y;

    if( repaint ) {
        this->doPaint();
    }
}

QWidget* Rule::parent() const
{
    return qobject_cast<QWidget*>( QObject::parent() );
}

double Rule::parentX( double x ) const
{
    return this->_x + x;
}

double Rule::parentY( double y ) const
{
    return this->_y + y;
}

double Rule::thisX( double x ) const
{
    return x - this->_x;
}

double Rule::thisY( double y ) const
{
    return y - this->_y;
}

double Rule::secDistance() const
{
    return static_cast<double>(_zoom) / 100 * sec_size;
}

double Rule::strokeDistance() const
{
    double distance = 0;
    for( int i = 0; i < section_value.size(); i++)
    {
        distance = static_cast<double>(_zoom) / 100 * sec_size * section_value[i];
        if( distance >= min_size )
            break;
    }
    return distance;
}

int Rule::strokeValue() const
{
    int stroke_value = 0;
    for( int i = 0; i < section_value.size(); i++ )
    {
        double distance = static_cast<double>(_zoom) / 100 * sec_size * section_value[i];
        if( distance >= min_size)
        {
            stroke_value = section_value[i];
            break;
        }
    }
    return stroke_value;
}

int Rule::captionMultipler() const
{
    int caption_multipler = 0;
    for( int i = 0; i < section_value.size(); i++ )
    {
        double distance = static_cast<double>(_zoom) / 100 * sec_size * section_value[i];
        if( distance > captionSize().width() + 10 )
        {
            caption_multipler = section_value[i];
            break;
        }
    }
    return caption_multipler;
}

QSize Rule::captionSize() const
{
    QFontMetrics fm( parent()->font() );
    return fm.size(Qt::TextSingleLine, QString("00:00"));
}

void Rule::doPaint()
{
    parent()->repaint();
}



