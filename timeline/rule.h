#ifndef RULE_H
#define RULE_H

#include <QObject>
#include <QPainter>


class Rule : public QObject
{
    Q_OBJECT
public:
    explicit Rule(QObject *parent = nullptr);
    void paint( QPainter& painter );
    void setDuration( int duration );
    void setPosition( int position );
    void setZoom( int zoom );
    void setGeometry( int x, int y, int w, int h );
    void resize( int w, int h );
    void move( int x, int y );
    int duration() const { return _duration; }
    int position() const { return _position; }
    int zoom() const { return _zoom; }
    int maxPosition() const;
    double msecToX( int msec ) const;
    int xToMsec( double x ) const;
    int visibleDuration() const;
    void setZeroShift( int zero_shift ) { _zero_shift = zero_shift; }
    int zeroShift() const { return _zero_shift; }
    void setLastShift( int last_shift ) { _last_shift = last_shift; }
    int lastShift() const { return _last_shift; }

private:
    void doPaint();
    void doResize( int w, int h, bool repaint );
    void doMove( int x, int y, bool repaint );
    QWidget* parent() const;
    double parentX( double x ) const;
    double parentY( double y ) const;
    double thisX( double x ) const;
    double thisY( double y ) const;
    double secDistance() const;
    double strokeDistance() const;
    int strokeValue() const;
    int captionMultipler() const;
    QSize captionSize() const;


    int _duration, _position, _zoom, _zero_shift, _last_shift;
    double _x, _y, _width, _height;
    QVector<int> section_value;

};

#endif // RULE_H
