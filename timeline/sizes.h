#ifndef SIZES_H
#define SIZES_H

static const int line_size = 1;
static const int border_size = 15;
static const int tool_width = 120;
static const int tool_height = 40;
static const int button_width = 18;
static const int button_height = 18;

#endif // SIZES_H
