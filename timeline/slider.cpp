#include "slider.h"
#include <QWidget>


Slider::Slider(QObject *parent) : QObject(parent)
{

}

void Slider::paint(QPainter &painter)
{

}


void Slider::setGeometry(double x, double y, double height)
{
    this->_x = x;
    this->_y = y;
    this->_height = height;

    this->doPaint();
}


void Slider::move(double x, double y)
{
    this->_x = x;
    this->_y = y;

    this->doPaint();
}


double Slider::parentX(double x) const
{
    return this->_x + x;
}

double Slider::parentY(double y) const
{
    return this->_y + y;
}

void Slider::doPaint()
{
    qobject_cast<QWidget*>( QObject::parent() )->repaint();
}
