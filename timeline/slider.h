#ifndef SLIDER_H
#define SLIDER_H

#include <QObject>
#include <QPainter>


class Slider : public QObject
{
    Q_OBJECT
public:
    explicit Slider(QObject *parent = nullptr);
    void paint( QPainter& painter );
    void setGeometry( double x, double y, double height );
    void move( double x, double y );
    double height() const { return _height; }
    double x() const { return _x; }
    double y() const { return _y; }

private:
    double parentX( double x ) const;
    double parentY( double y ) const;
    void doPaint();

    double _x, _y, _height;
};

#endif // SLIDER_H
