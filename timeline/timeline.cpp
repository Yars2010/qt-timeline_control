#include <QtCore/qmath.h>
#include <QDebug>
#include "timeline.h"
#include "sizes.h"
#include "rule.h"
#include "slider.h"
#include "area.h"
#include "track.h"
#include "action.h"
#include "slider.h"


QTimeLine::QTimeLine(QWidget *parent) :
    QWidget(parent), actions_left( false ), actions_right( false ), actions_center( false ), tools_center( false ), actions_selected(0), tools_selected(0),
        cursor_diff(0), editable_action(nullptr), h_scroll_bar(nullptr), v_scroll_bar(nullptr), plus_button(nullptr), minus_button(nullptr), rule(nullptr)
{
    this->Init();

    connect(plus_button, SIGNAL(pressed()), this, SLOT(zoomPlus()));
    connect(minus_button, SIGNAL(pressed()), this, SLOT(zoomMinus()));
    connect(h_scroll_bar, SIGNAL(sliderMoved(int)), this, SLOT(hScrollChanged(int)));
    connect(v_scroll_bar, SIGNAL(sliderMoved(int)), this, SLOT(vScrollChanged(int)));
}

void QTimeLine::Init()
{
    setMouseTracking(true);

    h_scroll_bar = new QScrollBar(this);
    h_scroll_bar->setOrientation(Qt::Horizontal);
    h_scroll_bar->hide();

    v_scroll_bar = new QScrollBar(this);
    v_scroll_bar->setOrientation(Qt::Vertical);
    v_scroll_bar->hide();

    plus_button = new QToolButton(this);
    plus_button->resize(button_width, button_height);
    plus_button->show();
    plus_button->setText( "+" );

    minus_button = new QToolButton(this);
    minus_button->resize(button_width, button_height);
    minus_button->show();
    minus_button->setText( "-" );

    rule = new Rule(this);
    rule->setDuration(60000);

    area = new Area(this);
    area->setRule( rule );
    connect( area, SIGNAL( changed() ), SLOT( areaChanged() ) );
}

void QTimeLine::setCurrentTime(int time)
{
    area->setCurrentTime( time );
}

int QTimeLine::currentTime() const
{
    return area->currentTime();
}

void QTimeLine::addTrack( Track* track )
{
    area->addTrack( track );
}

void QTimeLine::removeTrack( Track* track )
{
    area->removeTrack( track );
}

Track* QTimeLine::track( int index ) const
{
    return area->track( index );
}

int QTimeLine::tracksNumber() const
{
    return area->tracksNumber();
}

void QTimeLine::paintEvent(QPaintEvent* )
{
    QPainter painter;
    //painter.setRenderHint(QPainter::Antialiasing, true);
    painter.begin(this);

    area->paint( painter );
    rule->paint( painter);

    painter.end();
}

void QTimeLine::resizeEvent(QResizeEvent* )
{
    minus_button->move(width() - border_size - button_width * 2 ,height() - border_size - button_height);
    plus_button->move(width() - border_size - button_width, height() - border_size - button_height);
    v_scroll_bar->setGeometry(width() - border_size + line_size, border_size, border_size - line_size, height() - border_size * 2);
    h_scroll_bar->setGeometry(tool_width + border_size, height() - border_size + line_size, width() - (border_size * 2 + tool_width), border_size - line_size);
    rule->setGeometry(border_size + tool_width, 0, width() - (border_size * 2 + tool_width), border_size);
    area->setGeometry( border_size, border_size, width() - border_size * 2, height() - border_size * 2);
    //slider->setGeometry( rule->msecToX( current_time ), border_size, tool_height * area->tracksNumber() );

    this->updateScrollBars();
}

void QTimeLine::mouseMoveEvent(QMouseEvent *event)
{
    if( actions_center || actions_left || actions_right )
    {
        int val = rule->xToMsec(event->x() - cursor_diff);
        if     ( actions_center )
            editable_action->move( val );
        else if( actions_left )
            editable_action->setBegTime( val );
        else if( actions_right )
            editable_action->setEndTime( val );
    }
    else if( tools_center )
    {
        int pos = area->findTrack( movable_track );
        if( pos != 0 && ( event->y() - cursor_diff ) < ( tool_height * pos - tool_height / 2 ) )
        {
            area->removeTrack( movable_track );
            area->insertTrack( pos - 1, movable_track );
        }
        if( pos != area->tracksNumber() - 1 && ( event->y() - cursor_diff) > ( tool_height * pos + tool_height / 2 ) )
        {
            area->removeTrack( movable_track );
            area->insertTrack( pos + 1, movable_track );
        }
    }
    else
        updateCursors( event->x(), event->y() );
}

void QTimeLine::mousePressEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton )
    {
        QPointF cur( event->x(), event->y() );

        QList<QRectF> actions_rect = area->actionsRect();
        for( int i = 0; i < actions_rect.size(); ++i )
        {
            QRectF rect = actions_rect[ i ];
            if( ! rect.contains( cur ) ) {
                continue;
            }
            QRectF left_side( rect.x(), rect.y(), 5, rect.height() );
            QRectF right_side( rect.x() + rect.width() - 5 , rect.y(), 5, rect.height() );
            QRectF center( rect.x() + 5, rect.y(), rect.width() - 10, rect.height() );

            editable_action = area->action( i );

            if( tools_selected > 0 )
                allToolsUnselect();

            if( event->modifiers() != Qt::Modifier::CTRL)
            {
                if( actions_selected > 0 ) {
                    allActionsUnselect();
                }
                editable_action->setSelect( true );
                ++actions_selected;

                if( center.contains( cur ) || left_side.contains( cur ) )
                {
                    if( center.contains( cur ) )
                    {
                        actions_center = true;
                        setCursor( Qt::ClosedHandCursor );
                    }
                    else
                        actions_left = true;

                    cursor_diff = event->x() - rule->msecToX( editable_action->begTime() );
                }
                else if( right_side.contains( cur ) )
                {
                    actions_right = true;
                    cursor_diff = event->x() - rule->msecToX( editable_action->endTime() );
                }
            }
            else
            {
                int value = ! editable_action->select();
                editable_action->setSelect( value );

                if( value )
                    ++actions_selected;
                else
                    --actions_selected;
            }
            if( actions_selected > 0 ) {

            }

            return;
        }
        QList<QRectF> tools_rect = area->toolsRect();
        for( int i = 0; i < tools_rect.size(); ++i )
        {
            QRectF rect = tools_rect[ i ];
            if( ! rect.contains( cur ) ) {
                continue;
            }
            movable_track = area->track( i );

            if( actions_selected > 0 )
                allActionsUnselect();

            if( event->modifiers() != Qt::Modifier::CTRL )
            {
                if( tools_selected > 0 ) {
                    allToolsUnselect();
                }
                movable_track->setSelect( true );
                ++tools_selected;

                tools_center = true;
                cursor_diff = event->y() - tool_height * i;
                setCursor( Qt::ClosedHandCursor );
            }
            else
            {
                int value = ! movable_track->select();
                movable_track->setSelect( value );

                if( value )
                    ++tools_selected;
                else
                    --tools_selected;
            }
            if( tools_selected > 0 ) {

            }

            return;
        }
        QList<QRectF> lines_rect = area->linesRect();
        for( int i = 0; i < lines_rect.size(); ++i )
        {
            QRectF rect = lines_rect[ i ];
            if( ! rect.contains( cur ) ) {
                continue;
            }
            area->setCurrentTime( qFloor( rule->xToMsec( event->x() ) / 100 ) * 100 );
        }
        if( actions_selected > 0 )
            allActionsUnselect();
        if( tools_selected > 0 )
            allToolsUnselect();
    }
}

void QTimeLine::allActionsUnselect()
{
    for( int track_num = 0; track_num < area->tracksNumber(); ++track_num )
    {
        Track* track = area->track( track_num );
        for( int action_num = 0; action_num < track->actionsNumber(); ++action_num )
        {
            Action* action = track->action( action_num );
            action->setSelect( false );
        }
    }
    actions_selected = 0;
}

void QTimeLine::allToolsUnselect()
{
    for( int track_num = 0; track_num < area->tracksNumber(); ++track_num )
    {
        Track* track = area->track( track_num );
        track->setSelect( false );
    }
    tools_selected = 0;
}

void QTimeLine::mouseReleaseEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton )
    {
        tools_center = actions_center = actions_left = actions_right = false;
        updateCursors( event->x(), event->y() );
    }
}

QSize QTimeLine::sizeHint() const
{
    return QSize( border_size * 2 + tool_width * 2, border_size * 2 + tool_height * 2);
}

QSizePolicy QTimeLine::sizePolicy() const
{
    return QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void QTimeLine::areaChanged()
{
    this->updateScrollBars();
}

void QTimeLine::zoomPlus()
{
    rule->setZoom( rule->zoom() + 5 );
    this->updateScrollBars();
}

void QTimeLine::zoomMinus()
{
    rule->setZoom( rule->zoom() - 5 );
    this->updateScrollBars();
}

void QTimeLine::hScrollChanged(int value)
{
    rule->setPosition( value );
}

void QTimeLine::vScrollChanged(int value)
{
    area->setShift( value );
}


void QTimeLine::updateScrollBars()
{
    if( rule->maxPosition() > 0 )
    {
        h_scroll_bar->setRange(0, rule->maxPosition());
        h_scroll_bar->setPageStep( rule->visibleDuration() );

        if( h_scroll_bar->isHidden() ) {
            h_scroll_bar->show();
        }
    }
    else if( h_scroll_bar->isVisible()) {
        h_scroll_bar->hide();
    }

    if( area->maxShift() > 0 )
    {
        v_scroll_bar->setRange(0, area->maxShift() );
        v_scroll_bar->setPageStep( height() );

        if( v_scroll_bar->isHidden() ) {
            v_scroll_bar->show();
        }
    }
    else if( v_scroll_bar->isVisible() ) {
        v_scroll_bar->hide();
    }
}

void QTimeLine::updateCursors( int x, int y )
{
    QPointF cur( x, y );

    QRectF area_rect( area->x(), area->y(), area->width(), area->height() );
    if( area_rect.contains( cur ) )
    {
        QList<QRectF> actions_rect = area->actionsRect();
        for( int i = 0; i < actions_rect.size(); i++ )
        {
            QRectF rect = actions_rect[i];

            QRectF left_side( rect.x(), rect.y(), 5, rect.height() );
            QRectF right_side( rect.x() + rect.width() - 5 , rect.y(), 5, rect.height() );
            QRectF center( rect.x() + 5, rect.y(), rect.width() - 10, rect.height() );

            if( rect.contains( cur ) )
            {
                if( ( left_side.contains( cur ) || right_side.contains( cur ) ) && cursor() != Qt::SplitHCursor )
                    setCursor( Qt::SplitHCursor );
                else if( center.contains( cur ) && cursor() != Qt::OpenHandCursor )
                    setCursor( Qt::OpenHandCursor );
                return;
            }
        }

        QList<QRectF> tools_rect = area->toolsRect();
        for( int i = 0; i < tools_rect.size(); i++ )
        {
            QRectF rect = tools_rect[i];
            if( rect.contains( cur ) )
            {
                if( cursor() != Qt::OpenHandCursor )
                    setCursor( Qt::OpenHandCursor );
                return;
            }
        }

        QList<QRectF> lines_rect = area->linesRect();
        for( int i = 0; i < lines_rect.size(); i++ )
        {
            QRectF rect = lines_rect[i];
            if( rect.contains( cur ) )
            {
                if( cursor() != Qt::IBeamCursor )
                    setCursor( Qt::IBeamCursor );
                return;
            }
        }
    }
    if( cursor() != Qt::ArrowCursor )
        setCursor( Qt::ArrowCursor );
}
