#ifndef QTIME_LINE_H
#define QTIME_LINE_H

#include <QWidget>
#include <QList>
#include <QScrollBar>
#include <QPainter>
#include <QToolButton>
#include <QTimer>

class Rule;
class Slider;
class Area;
class Action;
class Track;


class QTimeLine : public QWidget
{
    Q_OBJECT

public:
    QTimeLine(QWidget *parent = nullptr);

    void Init();

    void setCurrentTime( int time );
    int currentTime() const;
    void addTrack( Track* track );
    void removeTrack( Track* track );
    Track* track( int index ) const;
    int tracksNumber() const;
    void allActionsUnselect();

    virtual void paintEvent(QPaintEvent* );
    virtual void resizeEvent(QResizeEvent* );
    virtual void mouseMoveEvent(QMouseEvent* event );
    virtual void mousePressEvent( QMouseEvent* event );
    virtual void mouseReleaseEvent( QMouseEvent* event );
    virtual QSize sizeHint() const;
    virtual QSizePolicy sizePolicy() const;

signals:
    void tracksSelected( QList<Track* > tracks );
    void actionsSelected( QList<Action* > actions );

private slots:
    void areaChanged();
    void zoomPlus();
    void zoomMinus();
    void vScrollChanged(int value);
    void hScrollChanged(int value);

private:
    void updateScrollBars();
    void updateCursors( int x, int y );
    void allToolsUnselect();

    //QList<Track*> tools_selected;
    //QList<Action*> actions_selected;
    bool actions_left, actions_right, actions_center, tools_center;
    int actions_selected, tools_selected;
    double cursor_diff;
    Track* movable_track;
    Action* editable_action;
    QScrollBar *h_scroll_bar, *v_scroll_bar;
    QToolButton *plus_button, *minus_button;
    Rule* rule;
    Area* area;

};

#endif // QTIME_LINE_H
