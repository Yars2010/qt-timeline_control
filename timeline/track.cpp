#include "track.h"
#include "action.h"
#include <QtAlgorithms>
#include <QWidget>


Track::Track() :
    QObject(nullptr), _actions(), _name(""), _select( false )
{
}

void Track::setSelect( bool select )
{
    _select = select;
    emit changed();
}

int Track::addAction( int pos, int duration, bool select )
{
    Action* action = new Action( pos, duration );
    action->setSelect( select );

    connect( action, SIGNAL( jumped( int, int ) ), SLOT( actionJumped( int, int ) ) );
    connect( action, SIGNAL( selected() ), SIGNAL( changed() ) );
    connect( action, SIGNAL( moved() ), SIGNAL( changed() ) );
    connect( action, SIGNAL( resized() ), SIGNAL( changed() ) );

    int curr_center = action->begTime() + action->duration() / 2;

    int insert_index = 0, item_center = 0;
    for( int i = 0; i < _actions.size(); i++ )
    {
        Action* item = _actions[i];
        item_center = item->begTime() + item->duration() / 2;

        if( curr_center < item_center )
            insert_index = i;
        else
            insert_index = i + 1;
    }
    _actions.insert( insert_index, action );
    action->setTrack( this );

    return insert_index;
}

int Track::findAction( const Action* action ) const
{
    QList<Action*>::const_iterator it = std::find( _actions.begin(), _actions.end(), action );
    if(it != _actions.end() )
        return it - _actions.begin();
    else
        return -1;
}

Action* Track::action( int num ) const
{
    return _actions[num];
}

void Track::removeAction( Action* action )
{
    int index = findAction( action );
    if( index >= 0 )
    {
        action->_track = nullptr;
        _actions.removeOne(action);

        disconnect( this, SLOT( actionJumped( int, int ) ) );
        disconnect( this, SIGNAL( changed() ) );
    }
}

int Track::actionsNumber() const
{
    return _actions.size();
}

void Track::actionJumped( int pos, int new_pos )
{
    Action* action = _actions[ pos ];
    _actions.removeAt( pos );
    _actions.insert( new_pos, action );

    emit changed();
}
