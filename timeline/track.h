#ifndef TRACK_H
#define TRACK_H

#include <QObject>
#include <QPainter>

class Action;
class Tool;
class Line;

class Track : public QObject
{
    Q_OBJECT
public:
    explicit Track();
    void setName( const QString& name ) { _name = name; }
    QString name() const { return _name; }

    void setSelect( bool select );
    bool select() const { return _select; }
    int addAction( int pos, int duration , bool select = false );
    int findAction( const Action* action ) const;
    Action* action( int num ) const;
    void removeAction( Action* action );
    int actionsNumber() const;

public slots:
    void actionJumped( int pos, int new_pos );

signals:
    void changed();

private:

    QList<Action*> _actions;
    QString _name;
    bool _select;

};

#endif // TRACK_H
